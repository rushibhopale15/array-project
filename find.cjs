
//function defination
function findFunction(temp, callBack) {
    if (Array.isArray(temp)) {
        for (let index = 0; index < temp.length; index++) {
            let checkCallBack =callBack(temp[index], index, temp);
            if(checkCallBack === true)
            {
                return temp[index];
            }
        }
    }
    return undefined;
}

module.exports = findFunction;

