//function defination
function filterFunction(temp, fun) {
    let newArr = [];
    if (Array.isArray(temp)) {
        for (let index = 0; index < temp.length; index++) {
            let checkCallBack = fun(temp[index], index, temp);
            if (checkCallBack === true) {
                newArr.push(temp[index]);
            }
        }
    }
    return newArr;
}

module.exports = filterFunction;