//function defination

function flattenFunction(temp, depth) {
    let flattenArray = [];
    if (depth === undefined) {
        depth = 1;
    }
    
        for (let index = 0; index < temp.length; index++) {

            if (Array.isArray(temp[index]) && depth > 0) {

                flattenArray = flattenArray.concat(flattenFunction(temp[index], depth-1));
            }
            else {
                if(temp[index] !== undefined){
                flattenArray.push(temp[index]);
                }
            }
        }
    return flattenArray;
}

module.exports = flattenFunction;