let findFunction = require('./find.cjs');


let items = [1, 2, 3, 4, 5, 5];
let callBack = (val) => {
    return val > 3;
}

let answer = findFunction(items, callBack);

console.log(answer);