
let flattenFunction = require('./flatten.cjs');

const nestedArray = [1, 2, [[3]], [[[4]]]]; 

// let ans = nestedArray.flat(2);
// console.log(ans);
let flattenArray= flattenFunction(nestedArray,2);

console.log(flattenArray);