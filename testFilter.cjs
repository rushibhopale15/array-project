let filterFunction = require('./filter.cjs');

//test case
let items = [1, 2, 3, 4, 5, 5];

//callback function
let callBack = (val) => {
    return val > 3;
}

//function call
let answer = filterFunction(items, callBack);

console.log(answer);
