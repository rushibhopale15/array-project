function reduceFunction(temp, callBack, startingValue) {

    if (Array.isArray(temp) && temp.length !== 0) {

        if (startingValue === undefined) {
            startingValue = temp[0];
            for (let index = 1; index < temp.length; index++) {
                startingValue = callBack(startingValue, temp[index],index,temp);
            }
        }
        else {
            for (let index = 0; index < temp.length; index++) {
                startingValue = callBack(startingValue, temp[index],index,temp);
            }
        }
    }

    return startingValue;
}


module.exports = reduceFunction;