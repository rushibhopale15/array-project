let mapFunction = require('./map.cjs');

//test array
const items = [1, 2, 3, 4, 5, 5];

//callback function
let callBack = (currVal) => {
    return currVal * 12;
}

//function call
let answer = mapFunction(items, callBack);
console.log(answer);

// console.log(items);  // uncomment to check items and mapArray is different 
