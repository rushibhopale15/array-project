
let eachFunction = require('./each.cjs');

//test Array
const items = [1, 2, 3, 4];

//callback function
let callBack = (currVal, index) => {
    console.log("currValue is " + currVal + " index is " + index);

}

//function call
eachFunction(items, callBack);
