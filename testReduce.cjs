let reduceFunction = require('./reduce.cjs')


let arr = [1, 2, 3, 4, 5, 5];

let callBack = (total,val)=>{
    return total + val
}

let answer = reduceFunction(arr, callBack);

console.log(answer);
