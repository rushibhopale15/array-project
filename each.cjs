
//function defination
function eachFunction(temp, callBack) {
    if (Array.isArray(temp)) {
        for (let index = 0; index < temp.length; index++) {
            callBack(temp[index], index, temp);
        }
    }
}

module.exports = eachFunction;

