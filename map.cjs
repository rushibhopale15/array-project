
//function defination
function mapFunction(temp, fun) {
    let newArr = [];  //map returns new array
    if (Array.isArray(temp)) {
        for (let index = 0; index < temp.length; index++) {
            newArr[index] = fun(temp[index], index, temp);
        }
    }
    return newArr;
}


module.exports = mapFunction;